class Vehicle {
  String? veh;
  String? type; //บก/น้ำ/บิน

  Vehicle(String veh, String type) {
    this.veh = veh;
    this.type = type;
  }
  void typeVeh() {
    print("The vehicle is : $veh");
  }

  void drive() {
    print("Able to drive on : $type");
  }

  String? getVeh() {
    return veh;
  }

  String? getType() {
    return type;
  }
}

class Motocycle extends Vehicle {
  Motocycle(String veh, String type) : super(veh, type);

  @override
  void typeVeh() {
    print("The vehicle is : $veh" + " | Able to drive on : $type");
  }
}

class Plane extends Vehicle {
  Plane(String veh, String type) : super(veh, type);

  void typeVeh() {
    print("The vehicle is : $veh" + " | Able to drive on : $type");
  }
}

void main(List<String> arguments) {
  Vehicle veh = new Vehicle("Bike", "land");
  veh.typeVeh();
  veh.drive();

  print("*******************");
  Motocycle motocycle = new Motocycle("Motocycle", "land");
  motocycle.typeVeh();

  print("*******************");
  Plane plane = new Plane("Plane", "Sky");
  plane.typeVeh();
}
